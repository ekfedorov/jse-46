package ru.ekfedorov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IGraphRepository;
import ru.ekfedorov.tm.model.SessionGraph;

import java.util.List;
import java.util.Optional;

public interface ISessionGraphRepository extends IGraphRepository<SessionGraph> {

    void clear();

    @NotNull
    List<SessionGraph> findAll();

    @NotNull
    Optional<SessionGraph> findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}
